# pprg
The project is being developed under python 3.7.4

How to use

python3 pyper.py -h for help

sample use >> python3 pyper.py HEP -a "author_name" -t "title_name"

Example query

python3 pyper.py HEP -a krzemien -t J-PET -d "2019->2020" -out file.txt -sort citations
